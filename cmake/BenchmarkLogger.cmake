set(bench_logger_compile_defs "")

CMAKE_DEPENDENT_OPTION(ENABLE_BENCHMARK_LOGGING "Enable benchmark logging" OFF
                        "BUILD_BENCHMARKS" OFF)
# set here since timer needs to have this defined always.
if (ENABLE_BENCHMARK_LOGGING)
  set(bench_logger_compile_defs "${bench_logger_compile_defs};-DBL_BENCHMARK")
endif(ENABLE_BENCHMARK_LOGGING)

CMAKE_DEPENDENT_OPTION(ENABLE_TIME_BENCHMARK "Enable Time Benchmarking" ON
                        "ENABLE_BENCHMARK_LOGGING" OFF)
if (ENABLE_TIME_BENCHMARK)
  set(bench_logger_compile_defs "${bench_logger_compile_defs};-DBL_BENCHMARK_TIME")
endif(ENABLE_TIME_BENCHMARK)

CMAKE_DEPENDENT_OPTION(ENABLE_MEMUSE_BENCHMARK "Enable Memory Usage Benchmarking" ON
                        "ENABLE_BENCHMARK_LOGGING" OFF)
if (ENABLE_MEMUSE_BENCHMARK)
  set(bench_logger_compile_defs "${bench_logger_compile_defs};-DBL_BENCHMARK_MEM")
endif(ENABLE_MEMUSE_BENCHMARK)

